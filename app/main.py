if __name__=="__main__":
    from tools.console import clear
    from chapitres.chap2_variables.exo2_11_1à3 import *
    from chapitres.chap3_affichage.exo3_6_1à5 import*
    from chapitres.chap4_liste.exo4_10_1à6 import*
    from chapitres.chap5_boucles_et_comparaisons.exo5_4_1à14 import*
    from chapitres.chap6_tests.exo6_7_1à10 import*

    def continu():
        continu= input("\nTapez 'Entrer' pour continuer")
        if continu==" ":
            return True
        else:
            return False

    while True:
        clear()
        print("""\nMenu de sélection:
        2- chapitre2:Variables
        3- chapitre3:Affichages
        4- chapitre4:Listes
        5- chapitre5:Boucles et Comparaisons
        6- chapitre6:Tests
        7- quitter""")

        choix = input("\nVeuillez entrer votre choix:")

        if choix == '2':
            num = '1'
            while (num!='4'):
                clear()
                print("""Vous êtes dans le chapitre 2.
    Voici les exercices dans ce chapitre:
        1-exo2_11_1:Friedman
        2-exo2_11_2:Opérations
        3-exo2_11_3:Opérations et conversions de types
        4-quitter""")
                num = input("\nVeuillez saisir le numéro d'exercice: ")
                if num == '1':
                    friedman()
                    continu()  
                elif num == '2':
                    operation()
                    continu()
                elif num == '3':
                    operation1()
                    continu()   

        elif choix == '3':
            num = '1'
            while num != '6':
                clear()
                print("""Vous êtes dans le chapitre 3.
    Voici les exercices dans ce chapitre:
        1- exo3_6_1: Affichage 
        2- exo3_6_2: Poly_A
        3- exo3_6_3: Poly_A et Poly_GC 
        4- exo3_6_4: Ecriture formatée
        5- exo3_6_5: Ecriture formatée2
        6- quitter""")
                num = input("\nVeuillez saisir le numéro d'exercice:")
                if num == '1':
                    Affichage()
                    continu()  
                elif num == '2':
                    poly_A()
                    continu()  
                elif num == '3':
                    Poly_A_poly_GC()
                    continu()   
                elif num == '4':
                    Ecriture_formatée()
                    continu()  
                elif num == '5':
                    Ecriture_formatee2()
                    continu()  

        elif choix == '4':
            num = '1'
            while num != '7':
                clear()
                print("""Vous êtes dans le chapitre 4. 
    Voici les exercices dans ce chapitre:")
        1- exo4_10_1: Jours de la semaine
        2- exo4_10_2: Saisons
        3- exo4_10_3: Table de multiplication par 9
        4- exo4_10_4: Nombres pairs
        5- exo4_10_5: List & Indice
        6- exo4_10_6: List & range
        7- quitter""")
                num = input("\nVeuillez saisir le numéro d'exercice:")
                if num == '1':
                    jours_de_la_semaine()
                    continu()            
                elif num == '2':
                    saisons()
                    continu()  
                elif num == '3':
                    table()
                    continu()  
                elif num == '4':
                    nombres_pairs()
                    continu()  
                elif num == '5':
                    list_indice()
                    continu()  
                elif num == '6':
                    list_range()
                    continu()  
        
        elif choix == '5':
            num = '1'
            while num != '15':
                clear()
                print("""Vous êtes dans le chapitre 5.
    Voici les exercices dans ce chapitre:
        1- exo5_4_1: Boucles de base
        2- exo5_4_2: Boucle et jours de la semaine
        3- exo5_4_3: Nombres 1 à 10 sur une ligne
        4- exo5_4_4: Nombres pairs et impairs
        5- exo5_4_5: Calcul de la moyenne
        6- exo5_4_6: Produits de nombres consécutifs
        7- exo5_4_7: Triangle
        8- exo5_4_8: Triangle inversé
        9- exo5_4_9: Triangle gauche
        10- exo5_4_10: Pyramide
        11- exo5_4_11: Parcours de matrice
        12- exo5_4_12: Parcours de demi-matrice sans la diagonale
        13- exo5_4_13: Sauts de puce
        14- exo5_4_14: Suite de Fibonacci
        15- quitter""")
                num = input("\nVeuillez saisir le numéro d'exercice:")
                if num == '1':
                    boucle_de_base()
                    continu()  
                elif num == '2':
                    boucle_et_joursDeLaSemaine()
                    continu()  
                elif num == '3':
                    nombre1à10()
                    continu()  
                elif num == '4':
                    nombres_pairs_et_impairs()
                    continu()  
                elif num == '5':
                    calculMoyenne()
                    continu()  
                elif num == '6':
                    produitNombresConsecutif()
                    continu()  
                elif num == '7':
                    triangle()
                    continu()  
                elif num == '8':
                    triangleInversé()
                    continu()  
                elif num == '9':
                    triangle_à_gauche()
                    continu()  
                elif num == '10':
                    pyramide()
                    continu()  
                elif num == '11':
                    parcoursMatrice()
                    continu()  
                elif num == '12':
                    parcoursDemi_matrice()
                    continu()  
                elif num == '13':
                    sautDePuce()
                    continu()  
                elif num == '14':
                    suiteFibonacci()
                    continu()  
            
        elif choix == '6':
            num = '1'
            while (num!='11'):
                clear()
                print("""Vous êtes dans le chapitre 6.
    Voici les exercices dans ce chapitre:
        1- exo6_7_1: Jours de la semaine
        2- exo6_7_2: Séquence complémentaire d'un brin ADN
        3- exo6_7_3: Minimum d'une liste
        4- exo6_7_4: Fréquence des acides aminés
        5- exo6_7_5: Notes et mention d'un étudiant
        6- exo6_7_6: Nombres pairs
        7- exo6_7_7: Conjecture de syracuse
        8- exo6_7_8: Attribution de la structure secondaire d'acide aminé d'une protéine
        9- exo6_7_9: Déterminatin des nombres premiers inférieur à 100
        10- exo6_7_10: Recherche d'un nombre dichotomie
        11- quitter""")
                num = input("\nVeuillez saisir le numéro d'exercice:")
                if num == '1':
                    joursSemaine()
                    continu()  
                elif num == '2':
                    sequenceComplemantaireADN()
                    continu()  
                elif num == '3':
                    minimunListe()
                    continu()  
                elif num == '4':
                    frequenceAcideAmine()
                    continu()  
                elif num == '5':
                    notesEtMention()
                    continu()  
                elif num == '6':
                    nombrePairs()
                    continu()  
                elif num == '7':
                    conjectureSyracuse()
                    continu()  
                elif num == '8':
                    structureSecondaireAcideAmine()
                    continu()  
                elif num == '9':
                    nombrePremier()
                    continu()  
                elif num == '10':
                    dichotomie()
                    continu()  
        
        elif choix == '7':
            break
        else:
            print("Choix invalide.Veuillez réessayer.")
            continu()
            

        

