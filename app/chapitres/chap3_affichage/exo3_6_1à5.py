#exo3_6_1
def Affichage():
    print("\nAffichage dans l'interpréteur et dans un programme :")
    print("L'istruction 1+1 dans un interpréteur python, il affiche tout de suite la réponse:\n>>> 1 + 1\n2")
    print(f"Dans un script test.py: le script s'exécute mais n'affiche rien pour cela, on a besoin d'utiliser la fonction print():print(1+1)={1+1}")

#exo3_6_2
def poly_A():
    print("\nPoly_A: \nUn brin d'ADN de 20 bases (A)")
    poly_A = "A"*20
    print(f"{poly_A}")

#exo3_6_3
def Poly_A_poly_GC():  
    print("\nPoly-A et poly-GC: \nUn brin d'ADN de 40 bases (A+GC) ")
    poly_A = 'A'*20
    poly_GC= 'GC'*20
    print(poly_A + poly_GC)

#exo3_6_4
def Ecriture_formatée():
    print("\nEcriture formatée: \nAffichage sur une seule ligne ")
    a="salut"
    b=102
    c=10.318
    print(f"{a}, {b}, {c:.2f}")

#exo3_6_5
def Ecriture_formatee2():
    print("\nEcriture formatée 2:\nCalcul pourcentage GC")
    perc_GC = ((4500 + 2575)/14800)*100
    print(f"""    Le pourcentage de GC est {perc_GC:.0f} % 
    Le pourcentage de GC est {perc_GC:.1f} %
    Le pourcentage de GC est {perc_GC:.2f} %
    Le pourcentage de GC est {perc_GC:.3f} %""")