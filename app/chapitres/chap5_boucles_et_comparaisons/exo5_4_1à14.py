#exo5_4_1
def boucle_de_base():  
    print("\nBoucles de base:")
    animaux = ["vache","souris","levure","bacterie"]
    print(animaux)
    print("Affichage des éléments de cette liste de trois façons différentes\
    (deux méthodes avec for et une avec while):")

    print("1er méthode:")
    animaux = ["vache","souris","levure","bacterie"]
    for animal in animaux :
        print(animal)
    print(f"-"*12)

    print("2ème méthode:")
    animaux = ["vache","souris","levure","bacterie"]
    for i in range(len(animaux)):
        print (animaux[i])
    print(f"-"*12)

    print("3ème méthode:")
    animaux = ["vache","souris","levure","bacterie"]
    i = 0
    while i < len(animaux):
        print(animaux[i])
        i = i + 1

#exo5_4_2
def boucle_et_joursDeLaSemaine():
    print("\nBoucle et jours de la semaine:")
    semaine = ["lundi", "mardi", "mercredi", "jeudi", "vendredi", "samedi", "dimanche"]

    print("Affichage des jours de la semaine")
    for jour in semaine:
        print(jour)

    print("\nAffichage du week-end")
    i = len(semaine) - 2
    while i < len(semaine):
        print(semaine[i])
        i = i + 1

#exo5_4_3
def nombre1à10():
    print("\nNombres de 1 à 10 sur une ligne:")
    print("Affichage des nombres de 1 à 10 sur une seule ligne:")
    for i in range(1,11):
        print(i,end=" ")

#exo5_4_4
def nombres_pairs_et_impairs():
    print("\nNombres pairs et impairs:")
    Impairs =  [1, 3, 5, 7, 9, 11, 13, 15, 17, 19, 21]
    print("Nombre impairs:",Impairs)
    Pairs = list(range(2,23,2))
    print ("Nombre pairs:",Pairs)

#exo5_4_5
def calculMoyenne():
    print("\nCalcul de la moyenne:")
    notes =  [14, 9, 6, 8, 12]
    print("Voici les notes d'un étudiant:",notes)
    somme = 0
    moyenne = 0
    i = 0
    while i < len(notes):
        somme += notes[i]
        i = i + 1

    moyenne = somme/len(notes)
    print(f"Moyenne = {moyenne: .2f}")

#exo5_4_6
def produitNombresConsecutif():
    print("\nProduit de nombres consécutifs:")
    entiers = list(range(2,21,2))
    print("Liste des entiers contenant les nombres entiers pairs de 2 à 20:\n",entiers)

    print("Le produit des nombres consécutifs deux à deux des entiers:")
    for i in range(len(entiers)-1):
        print(f"{entiers[i]*entiers[i+1]}")
        i += 1  

#exo5_4_7
def triangle():
    print("\nTriangle:")
    n = 10
    for i in range (1, n+1):
        print(f"{'*'*i}")

#exo5_4_8
def triangleInversé():
    print("\nTriangle inversé:")
    n = 10
    for i in range(n, 0,-1):
        print(f"{'*'*i}")

#exo5_4_9
def triangle_à_gauche():
    print("\nTriangle à gauche:")
    n =10
    for i in range(1,n+1):
        print(f"{' '*(n-i)}{'*'*i}")

#exo5_4_10
def pyramide():
    print("\nPyramide:")
    while True:
        try:
            ligne = int(input("Entrer le nombre de lignes (entier positif): "))
            if ligne > 0:
                break
            else:
                print("Veuillez entrer un nombre entier positif.")
        except ValueError:
            print("Veuillez entrer un nombre entier valide.")

    etoile = 1
    for i in range(0, ligne):
        print(" " * (ligne - i), "*" * etoile)
        etoile += 2

#exo5_4_11
def parcoursMatrice():
    print("\nParcours de matrice:")

    while True:
        try:
            dim = int(input("Entrez la dimension de la matrice carrée (entier positif) : "))
            if dim > 0:
                break
            else:
                print("Veuillez entrer un nombre entier positif.")
        except ValueError:
            print("Veuillez entrer un nombre entier valide.")

    for i in range(1, dim + 1):
        for j in range(1, dim + 1):
            print(f"{i:4} {j:4}")

#exo5_4_12
def parcoursDemi_matrice():
    print("\nParcours de demi-matrice sans la diagonale:")

    while True:
        try:
            N = int(input("Entrez le nombre de lignes (entier positif) : "))
            if N > 0:
                break
            else:
                print("Veuillez entrer un nombre entier positif.")
        except ValueError:
            print("Veuillez entrer un nombre entier valide.")

    tab = [[0 for val in range(N)] for val in range(N)]
    print(f"tab(tab)")

    print("\n ligne colonne")
    i, counter = 0, 0
    while i < len(tab):
        j = 0
        while j < len(tab[i]):
            while j < i and j != i:
                print(f"{i+1:>4d}{j+1:>5d}")
                counter += 1
                break
            j += 1
        i += 1

    print(f"Pour une matrice {N}*{N}, on a parcouru {counter} cases\n")
    print("La formule générale reliant le nombre de cases parcourues à N est :")
    print("Nombre total de cases parcourues = [N x (N-1)]/2")


#exo5_4_13
def sautDePuce():
    print("\nSauts de puce:")
    import random
    puce,count = 0,0
    while not puce ==5:
        puce += random.choice([-1,1]);print(f"{puce}", end="_")
        count += 1
    print(f"\n Nombre de saut de 0 à 5:{count}")

#exo5_4_14
def suiteFibonacci():
    print("\nSuite de Fibonacci:")
    fibo = [0,1]

    print(f"Les 15 premiers termes de la suite de Fibonacci sont \n {fibo[0]}{fibo[1]}", end= " ")
    i = len(fibo)
    while i <= 15:
        fibo += [fibo[i-1]+fibo[i-2]]
        print(fibo[i], end=" ")
        i+=1
    print("\n\nRapport entre l'element de rang n et n-1 sur les 15 premiers termes de la suite de fibonacci:")
    for i in range(2, len(fibo)):
            ratio = fibo[i] / fibo[i-1]
            print(f"Rapport entre {fibo[i]} et {fibo[i-1]} : {ratio}")
    print("Lorsque les termes augmentent, le rapport tend vers la valeur constante ϕ (phi) appelée le nombre d'or, qui est d'environ 1.61")

