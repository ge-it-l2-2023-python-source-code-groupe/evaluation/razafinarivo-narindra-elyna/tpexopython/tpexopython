#exo4_10_1
def jours_de_la_semaine():   
    print("\nJours de la semaine:")
    semaine = ["lundi","mardi","mercredi","jeudi","vendredi","samedi","dimanche"]
    print (f"La liste des jours de la semaine:{semaine}")
    print(f"Les 5 premiers jours de la semaine:{semaine[0:5]}")
    print(f"Les jours du week-end:{semaine[5:]}")
    print(f"Une autre méthode:\nLes 5 premiers jours de la semaine:{semaine[-7:-2]}\nLes jours de week-end:{semaine[-2:]}")
    print(f"La première méthode pour accéder au dernier jour de la semaine:{semaine[6:]}")
    print(f"La deuxième méthode:{semaine[-1:]}")
    print(f"Inversion des jours de la semaine:{semaine[-1:-8:-1]}")

#exo4_10_2
def saisons():
    print("\nSaisons:")
    hiver = ["Décembre","Janvier","Février"]
    printemps = ["Mars","Avril","Mai"]
    ete = ["Juin","Juillet","Août"]
    automne = ["Septembre","Octobre","Novembre"]
    saisons = [hiver, printemps, ete, automne]
    print(saisons[2])
    print(saisons[1][0])
    print(saisons[1:2])
    print(f"L'instruction :saisons[:][1] accède au deuxième élément de la liste saisons: {saisons[:][1]}")

#exo4_10_3
def table():
    print("\nTable de multiplication par 9:")
    produit = list(range(0,91,9))
    print(f"La table de 9 est:{produit}",end =" ")

#exo4_10_4
def nombres_pairs():
    print("\nNombres pairs:")
    pairs = len(range(2,10001,2))
    print(f"Il y a {pairs} nombres pairs dans l'intervalle[2, 10000]")

#exo4_10_5
def list_indice():
    print("\nList & indice:")
    semaine = ["lundi","mardi","mercredi","jeudi","vendredi","samedi","dimanche"]
    print (f"Affichage de la liste semaine:{semaine}")

    print (f"La valeur de semaine[4]:{semaine[4]}")

    temp = semaine[0]
    semaine[0] = semaine[6]
    semaine[6] = temp
    print (f"Echange de la valeur de la première et de la dernière case de la liste:{semaine}")

    print(f"Affichage de la valeur du dernier élément de la liste 12 fois:{semaine[6]*12}")

#exo4_10_6
def list_range():
    print("\nList & range:")
    lstVide, lstFlottant = list(), [0,0]*5
    print("Affichage lstVide et lstFlottant:")
    print(lstVide)
    print(lstFlottant)
    print(f"{'-'*12}")
    lstVide += range(0,1001,200)
    print(f"Ajout des nombres entre 0 et 1000 avec « step » 200 à la liste vide lstVide:{lstVide}")
    print(f"{'-'*12}")
    print(f"Les entiers de 0 à 3: range(4) = {list(range(4))}")
    print(f"Les entiers de 4 à 7: range(4,8) = {list(range(4,8))}")
    print(f"Les entiers de 2 à 8 par pas de 2: range(2, 9, 2) = {list(range(2, 9, 2))}")
    print(f"{'-'*12}")
    lstElmnt = list(range(6)) + lstVide + lstFlottant
    print(f"Une liste des entiers de 0 à 5 plus le contenu des deux listes():{lstElmnt}") 