#exo6_7_1
def joursSemaine():  
    print("\nJours de la semaine:")
    print("\nAffichage de messages en utilisant des test\n")
    semaine=['lundi','mardi','mercredi','jeudi','vendredi','samedi','dimanche']
    print(semaine)
    print('*'*24)
    for jour in semaine:
        if jour in semaine[:4] :
            print(f"{jour}, Au travail.")  
        elif jour == semaine[4]:
            print(f"Chouette c'est {semaine[4]}.")  
        else:
            print(f"Le {jour}, Repos ce week-end.")
    
#exo6_7_2
def sequenceComplemantaireADN():
    print("\nSéquence complémentaire d'un brin d'ADN:")
    sequence=['A','C','G','T','T','A','G','C','T','A','A','C','G']
    print(sequence)
    print("Voici la séquence complémentaire :")
    for i in range(len(sequence)):
        if sequence[i]=='A':
            print('T',end='_')
        elif sequence[i]=='T':
            print('A',end='_')
        elif sequence[i]=='C':
            print('G',end='_')
        else:
            print('C',end='_')

#exo6_7_3
def minimunListe():
    print("\nMinimum d'une liste:")
    liste=[8,4,6,1,5]
    print(liste)
    min = liste[0]
    for i in liste:
        min = i if min > i else min
    print(f"Le minimum dans cette liste est:{min}")

#exo6_7_4
def frequenceAcideAmine():
    print("\nFréquence des acides aminés")
    AA=['A','R','A','W','W','A','W','A','R','W','W','R','A','G']
    print(AA)
    Alanine,Arginine,Trytophane,Glycine = 0,0,0,0
    for i in AA:
        if i == 'A':
            Alanine += 1
        elif i == 'R':
            Arginine += 1
        elif i == 'W':
            Trytophane += 1
        elif i == 'G':
            Glycine += 1
    print(f"Les fréquences:\n   Alanine (A):{Alanine}\n   Arginine (R):{Arginine}\n   Trypophane (W):{Trytophane}\n   Glycine (G):{Glycine}")

#exo6_7_5
def notesEtMention():
    print("\nNotes et mentions d'un étudiant:")
    note=[14,9,13,15,12]
    print(f"Notes:{note}")
    print(f"maximum de notes:{max(note)}\nminimum de notes:{min(note)}")
    moyen = sum(note)/len(note)
    print(f"moyenne de notes:{moyen:.2f}")
    if 10 <= moyen <12:
        print("Mention:Passable!")
    elif 12 <= moyen <14:
        print(" Mention:Assez bien!")
    elif moyen <= 14:
        print("Mention:Bien!")

#exo6_7_6
def nombrePairs():
    print("\nNombres pairs:")
    print("Les nombres pairs inférieurs ou égaux à 10 d'une part et les nombres impairs strictement supérieurs à 10 d'une part")
    for i in range(21):
        if i % 2 == 0 and i <= 10:
            print(i, end=" ")
        elif i % 2 != 0 and i > 10:
            print(i, end=" ")

#exo6_7_7
def conjectureSyracuse():
    print("\nConjecture de Syracuse:")

    while True:
        try:
            n = int(input("Entrez un nombre (entier positif) : "))
            if n > 0:
                break
            else:
                print("Veuillez entrer un nombre entier positif.")
        except ValueError:
            print("Veuillez entrer un nombre entier valide.")

    syracuse = [n]
    trivial = []

    while len(syracuse) < 50:
        n = n // 2 if n % 2 == 0 else n * 3 + 1
        syracuse += [n]
        if n == 1:
            trivial = syracuse[-3:]

    print(syracuse)
    print(f"Oui! La conjecture de Syracuse est vérifiée.\nLes nombres qui constituent le cycle trivial sont : {trivial}")

        
#exo6_7_8
def structureSecondaireAcideAmine():
    print("\nAttribution de la structure secondaire des acides aminés d'une protéine:")
    proteine_ITFE = [
    [48.6 , 53.4] ,[ -124.9 , 156.7] ,[ -66.2 , -30.8], 
    [ -58.8 , -43.1] ,[ -73.9 , -40.6] ,[ -53.7 , -37.5] ,
    [ -80.6 , -26.0] ,[ -68.5 , 135.0] ,[ -64.9 , -23.5] ,
    [ -66.9 , -45.5] ,[ -69.6 , -41.0] ,[ -62.7 , -37.5] ,
    [ -68.2 , -38.3] ,[ -61.2 , -49.1] ,[ -59.7 , -41.1]
    ]
    print("List des acides aminés de la protéine ITFE:")
    for acide_amine in proteine_ITFE:
        phi, psi = acide_amine[0], acide_amine[1]
        result = "est en helice" if abs(-57 - phi)<= 30 and abs(-47 - psi)<= 30 else "n'est pas en helice"
        print(f"{acide_amine}{result}")
    print("La structure secondaire majoritaire de ces 15 acides aminés est en helice.")

#exo6_7_9
def nombrePremier():
    print("\nDétermination des nombres premiers inférieurs à 100:")
    print(f"Les nombres premiers inférieurs à 100 sont:")
    print("1er méthode:")
    limite = 100
    nbre, count = 2,0
    if nbre > 1:
        while nbre <= limite:
            if nbre == 2:
                count +=1; print(nbre, end=" ")
            else:
                i,diviseur = 1,0
                while i <= nbre:
                    if nbre % i == 0:
                        diviseur +=1
                    i+=1
                if diviseur <= 2:
                    count += 1 ; print(nbre,end=" ")
            nbre += 1
    print(f"\nEntre 0 et 100 il y a {count} nombre premier.")
    print("2ème méthode:")
    ispremier = False
    premier = [2]
        
    for i in range(3,100):
        for j in premier:
            if (i % j == 0):
                ispremier=False
                break
            else:
                ispremier=True
        if ispremier:
            premier += [i]
    print(premier)
    print(f"Il y a {len(premier)} nombre premier entre 0 et 100.")

#exo6_7_10
def dichotomie():
    print("\nRecherche d'un nombre par dichotomie:")
    print(f"Pensez un nombre entre 1 et 100.")
    a=1
    b=99
    i=1
    while a <= b:
        Devinette = (a + b)/2
        reponse = input(f"Est ce que votre nombre est plus grand ou plus petit ou égal à {Devinette} ? [+/-/=]")
        if reponse == '+':
            a = Devinette + 1
        elif reponse == '-':
            b = Devinette - 1
        elif reponse == '=':
            print(f"C'est génial! Je viens de trouver en {i} questions votre nombre.")
            break
        else:
            print(f"Si votre nombre est supérieur,taper + \nSi votre Si votre nombre est inférieur,tapez -\nEt si votre nombre est égal à ce npmbre,tapez = ")
        i += 1

