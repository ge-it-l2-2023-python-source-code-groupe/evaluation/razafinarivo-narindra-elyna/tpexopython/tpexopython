#exo2_11_1
def friedman():
    print(f"\nTest d'expression mathématique:\nL'expression 7+3\u2076 = {7+3**6} --> est un nombre de friedman.")
    print(f"L'expression (3+4)\u2073 = {(3+4)**3} --> est un nombre de friedman.")
    print(f"L'expression 3\u2076-5 = {3**6-5} --> n'est pas un nombre de friedman.")
    print(f"L'expression (1+2\u2078)*5 = {(1+2**8)*5} --> est un nombre de friedman.")
    print(f"L'expression (2+1\u2078)\u2077 = {(2+1**8)**7} --> est un nombre de friedman.")

#exo2_11_1
def operation():
    print("\nOpérations:")
    print(f"La première opération:(1+2)\u2073 = {(1+2)**3}")
    print(f"La deuxième opération: 'Da'*4 = {"Da"*4}")
    print(f"La troisième opération 'Da' + 3 n'est pas valide ")
    print(f"La quatrième opération: ('Pa'+'La') * 2 = { ("Pa"+"La") * 2}")
    print(f"La cinquième opération: ('Da'*4) / 2 n'est pas valide ")
    print(f"La sixième opération: 5 / 2 = { 5 / 2}")
    print(f"La septième opération: 5 // 2 = { 5 // 2}")
    print(f"La huitième opération:5 % 2 = { 5 % 2}")

#exo2_11_1
def operation1():
    print("\nOpérations et conversions de types:")
    print(f"Première instruction : str(4) * int('3') = {str(4) * int("3")}")
    print(f"Deuxième instruction : int('3') + float('3.2') = {int("3") + float("3.2")}")
    print(f"Troisième instruction : str(3) * float('3.2'): invalide")
    print(f"Quatrième instruction : str(3/4) * 2 = {str(3/4) * 2}")
